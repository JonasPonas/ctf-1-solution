#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <winsock2.h>
#include <time.h>

//#pragma comment(lib, "ws2_32.lib")

int main(int argc, char const *argv[])
{
    WSADATA wsaData;
    int RetCode;

    const char *sendbuf = "Please";
    char recvbuf[2048];

    // Initialize Winsock version 2.2
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    printf("Client: Winsock DLL status is %s.\n", wsaData.szSystemStatus);

    SOCKADDR_IN ServerAddr, ThisSenderInfo;
    SOCKET SendingSocket;

    ServerAddr.sin_family = AF_INET;
    ServerAddr.sin_addr.s_addr = inet_addr("78.56.77.230");

    for (int i = 0; i < 4; i++)
    {
        SendingSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

        ServerAddr.sin_port = htons(8124 + i);
        RetCode = connect(SendingSocket, (SOCKADDR *)&ServerAddr, sizeof(ServerAddr));
        RetCode = send(SendingSocket, sendbuf, (int)strlen(sendbuf), 0);
        printf("Data Sent: %s, to port: %d\n", sendbuf, 8124 + i);
        if (i < 3)
        {
            printf("Closing Port: %d\n", 8124 + i);
        }
        else
        {
            do
            {
                RetCode = recv(SendingSocket, recvbuf, sizeof(recvbuf), 0);
                if (RetCode > 0)
                {
                    //recvbuf[strlen(recvbuf)+1]='\0';
                    printf("Data Received: %s\n", recvbuf);
                }
                else if (RetCode == 0)
                    printf("Connection closed\n");
                else
                    printf("recv failed with error: %d\n", WSAGetLastError());

            } while (RetCode > 0);
        }
    }

    strtok(recvbuf, "FLAG:");
    printf("The First Flag is: %s", strtok(NULL,"FLAG:"));

    // cleanup
    closesocket(SendingSocket);
    WSACleanup();

    return 0;
}