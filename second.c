#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <winsock2.h>
#include <time.h>

//#pragma comment(lib, "ws2_32.lib")

void findFlag(){
    FILE *fptr = fopen("decoded.txt", "r");
    char c;
    char wordBuf[512];
    int pos = 0;
    while ((c = fgetc(fptr)) != EOF)
    {
        if (c == ' ' || c == '\n')
        {
            if (strstr(wordBuf, "FLAG:"))
            {
                printf("%s\n", strtok(wordBuf, "C"));
            }

            memset(&wordBuf, 0, sizeof(wordBuf));
            pos = 0;
        }
        else
        {
            wordBuf[pos] = c;
            pos++;
        }
    }
    fclose(fptr);
}

int main(int argc, char const *argv[])
{
    char line[512][12];
    int i = 0;

    FILE *fptr = fopen("encoded.txt", "r");
    while (fgets(line[i], 11, fptr))
        i++;

    fclose(fptr);

    WSADATA wsaData;
    int RetCode;
    char recvbuf[1024];

    // Initialize Winsock version 2.2
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    printf("Client: Winsock DLL status is %s.\n", wsaData.szSystemStatus);

    SOCKADDR_IN ServerAddr, ThisSenderInfo;
    SOCKET SendingSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    ServerAddr.sin_family = AF_INET;
    ServerAddr.sin_addr.s_addr = inet_addr("78.56.77.230");
    ServerAddr.sin_port = htons(5555);
    RetCode = connect(SendingSocket, (SOCKADDR *)&ServerAddr, sizeof(ServerAddr));

    fptr = fopen("decoded.txt", "w+");
    for (int j = 0; j < i; j++)
    {
        RetCode = send(SendingSocket, line[j], (int)strlen(line[j]), 0);
        //printf("Sending Data: %s\n", line[j]);
        RetCode = recv(SendingSocket, recvbuf, sizeof(recvbuf), 0);
        if (RetCode > 0)
        {
            //printf("Data Received: %s\n", recvbuf);
            fputs(recvbuf, fptr);
            memset(&recvbuf, 0, sizeof(recvbuf));
        }
        else if (RetCode == 0)
        {
            printf("Connection Closed!\n");
            break;
        }
    }
    fclose(fptr);

    // cleanup
    closesocket(SendingSocket);
    WSACleanup();

    findFlag();

    return 0;
}